post({
    "title": "Validating Modulo11 numbers",
    "code": "validating-modulo11-numbers",
    "author": "Tomas Hubelbauer",
    "tags":
    [
        "Miscelaneous"
    ],
    "dateAndTime":
    {
        "year": 2016,
        "month": 3,
        "day": 16,
        "hour": 18,
        "minute": 40,
        "second": 0
    },
    "contents":
    [
        { "perex": "Modulo11 is a format of bank account numbers used in Czech Republic and possibly elsewhere. Here's an easy way to check if a number is Modulo11 compliant using C#." },
        { "p": "Rhaeo uses the services of Czech bank called Fio. One neat thing Fio allows you to do while creating an account is to choose the account number you desire as long as it is still free, so no luck if you'r dream bank account number was already assigned to someone else. Another condition that must be met is that the number must be Modulo11 scheme compliant. This scheme is a variant of common check digit algorithm and works in such a way that your desired 8-10 digit number's digits are multiplied by constant defined weights, summed and modulo 11 of the sum is then obtained. If it is zero, the number is valid Modulo11 number. While checking to see if our favorite numbers could be obtained for our new bank account, I've come up with a script that allowed us to check this quickly." },
        {
            "snippet-cs":
            [
                "void Main()",
                "{",
                "    (IsValidModulo11Number(1234567899) ? \"Valid Modulo11 number.\" : \"Invalid.\").Dump();",
                "}",
                "",
                "int[] weights = new[] { 1, 2, 4, 8, 5, 10, 9, 7, 3, 6 };",
                "",
                "bool IsValidModulo11Number(long number) => GetDigits(number).Select((digit, index)",
                "    => digit * weights[index]).Sum() % 11 == 0;",
                "",
                "IEnumerable<int> GetDigits(long number)",
                "{",
                "    do yield return (int)(number % 10); while ((number /= 10) > 0);",
                "}",
                ""
            ]
        }
    ]
});
