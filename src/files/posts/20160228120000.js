post({
    "title": "Hello Again, Rhaeo!",
    "code": "hello-again-rhaeo",
    "author": "Tomas Hubelbauer",
    "tags":
    [
        "Miscelaneous"
    ],
    "dateAndTime":
    {
        "year": 2016,
        "month": 2,
        "day": 28,
        "hour": 12,
        "minute": 0,
        "second": 0
    },
    "contents":
    [
        { "perex": "Rhaeo is a software development and integrations company based in Czech Republic. Mail us or call us to get a quote on your desktop or mobile application project as well as a medium scale industry integration and automation project." },
        { "p": "We're excited to annouce Rhaeo is open to new opportunities come January 2017."  }
    ]
});
