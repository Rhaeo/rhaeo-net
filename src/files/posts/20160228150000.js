post({
    "title": "Using Shared Projects in Visual Studio",
    "code": "using-shared-projects-in-visual-studio",
    "author": "Tomas Hubelbauer",
    "tags":
    [
        "Miscelaneous"
    ],
    "dateAndTime":
    {
        "year": 2016,
        "month": 2,
        "day": 28,
        "hour": 15,
        "minute": 0,
        "second": 0
    },
    "contents":
    [
        { "perex": "Visual Studio offers an intuitive new way to structure logical parts of your solution without dividing them up to different assemblies." },
        {
            "p":
            [
                "Recently when building a mobile application for Windows Phone, I've faced a problem which surprised me. ",
                "Usually, when building applications using the MVVM pattern, as was the case here as well, I create a separate assembly for models as well as views for easier management. ",
                "I did the same here, but when running the application in either the simulator or on the device, it would crash with the same exception: ",
                { "i": "Error HRESULT E_FAIL has been returned from a call to a COM component." }
            ]
        },
        {
            "p":
            [
                "I wasn't going to be able to figure out such a generic exception on my own and since the application was still barely worked on, it was basically a project scaffolded from the template with very little changes applied to it, I didn't expect jumping into the platform code was necessary; surely, someone must have encountered this issue before. ",
                "Of course googling the exception was to no avail, so I ",
                { "a": "opted to ask ", "url": "http://stackoverflow.com/questions/28071841/frame-navigate-to-a-page-derived-class-in-a-different-assembly" },
                "a question on Stack Overflow and described my issue."
            ]
        },
        {
            "p":
            [
                "I didn't get any feedback on my question, turns out separating views to their own assembly is uncommon enough so that no other developer who has seen the question has encountered the same problem. ",
                "I wasn't going to give up that easily, though, I wouldn't be too bothered having the views in the entry assembly, but I also saw value in the separation approach and I found it odd that it worked fine on desktop for all these years. ",
                "I started to dig to see if I could get a workaround going and I was surprised how quickly one presented itself."
            ]
        },
        { "h2": "Shared Projects" },
        {
            "p":
            [
                "When opening the ",
                { "i": "Add new project " },
                "window in Visual Studio, if you look hard enough or search, you will find a template caled ",
                { "i": "Shared Project" },
                ". ",
                "This project type looks very close to normal library project in the Solution Explorer, but there's a significant difference. ",
                "If you reference this project from other projects, from the compiler's point of view, it will seems as though the source files in the Shared Project are in the project, that references the shared project. ",
                "Very neat, because this way I could overcome the bug in the platform and have the compiler think the views were in the entry assembly when source-wise, they actually weren't."
            ]
        },
        { "h2": "Gotchas & Takeaways" },
        { "p": "While Shared Projects are very cool, there's a couple of things to keep in mind:" },
        {
            "ul":
            [
                "If the IntelliSense doesn't seem to be working in the Shared Project, make sure you referenced it from somewhere. Shared Project has no concept of the base class library or anything, really, on it's own, so you won't get more than syntax highlighting, unless you reference it from elsewhere.",
                "If you like F#, you'll be disappointed to learn that this is yet another case of neglected tooling for F#. Shared Projects won't work.",
                "When exploring a platform you have little prior experience with, it pays to learn how do the more complex templates work. I was using the Empty Project template, had I used a more opinionated Universal template, I would have learned about Shared Projects long ago."
            ]
        }
    ]
});
