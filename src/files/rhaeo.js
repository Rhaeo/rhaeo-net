// https://gist.github.com/atk/1034464
Object.keys = Object.keys || function(o, k, r) { r=[]; for(k in o) r.hasOwnProperty.call(o,k) && r.push(k); return r; };

// https://gist.github.com/atk/1034882
Array.isArray || (Array.isArray = function(a) { return '' + a !== a && {}.toString.call(a) == '[object Array]'; });

function post(post)
{
    var articleElement = document.createElement("article");
    
    var h1Element = document.createElement("h1");
    articleElement.appendChild(h1Element);
    
    var titleAElement = document.createElement("a");
    titleAElement.setAttribute("href", "#" + post.code);
    titleAElement.setAttribute("name", post.code);
    titleAElement.appendChild(document.createTextNode(post.title));
    h1Element.appendChild(titleAElement);
    
    var divMetaElement = document.createElement("div");
    divMetaElement.setAttribute("class", "blog-meta");
    articleElement.appendChild(divMetaElement);
    
    var scrollToTopAElement = document.createElement("a");
    scrollToTopAElement.setAttribute("class", "scrollToTopA");
    scrollToTopAElement.setAttribute("href", "#");
    scrollToTopAElement.setAttribute("title", "Scroll to top");
    scrollToTopAElement.appendChild(document.createTextNode("\u21e1"));
    divMetaElement.appendChild(scrollToTopAElement);
    divMetaElement.appendChild(document.createTextNode(" \u00b7 "));
    
    var authorAElement = document.createElement("a");
    authorAElement.appendChild(document.createTextNode(post.author));
    divMetaElement.appendChild(authorAElement);
    divMetaElement.appendChild(document.createTextNode(" \u00b7 "));
    
    var tags = post.tags;
    for (var tagIndex = 0; tagIndex < tags.length; tagIndex++)
    {
        var tag = tags[tagIndex];
        var tagAElement = document.createElement("a");
        tagAElement.appendChild(document.createTextNode(tag));
        divMetaElement.appendChild(tagAElement);
        divMetaElement.appendChild(document.createTextNode(" \u00b7 "));
    }
    
    var dateAndTimeAElement = document.createElement("time");
    dateAndTimeAElement.appendChild(document.createTextNode(post.dateAndTime.year + "/" + post.dateAndTime.month + "/" + zero(post.dateAndTime.day) + " " + zero(post.dateAndTime.hour) + ":" + zero(post.dateAndTime.minute) + ":" + zero(post.dateAndTime.second)));
    divMetaElement.appendChild(dateAndTimeAElement);
    
    var divDataElement = document.createElement("div");
    divDataElement.setAttribute("class", "blog-data");
    articleElement.appendChild(divDataElement);
    
    curse(post.contents, divDataElement);
    
    document.getElementById("blog").appendChild(articleElement);
}

function zero(value)
{
    return value < 10 ? "0" + value : value;
}

function curse(contents, container, textElementDefault)
{
    var items = contents;
    for (var itemIndex = 0; itemIndex < items.length; itemIndex++)
    {
        var item = items[itemIndex];
        if (typeof item === "string" || item instanceof String)
        {
            if (textElementDefault)
            {
                var textElement = document.createElement(textElementDefault);
                textElement.appendChild(document.createTextNode(item));
                container.appendChild(textElement);
            }
            else
            {
                container.appendChild(document.createTextNode(item));
            }
        }
        else if (typeof item === "object")
        {
            var keys = Object.keys(item);
            if (keys.length === 1)
            {
                if (keys[0] === "perex")
                {
                    if (Array.isArray(item["perex"]))
                    {
                        console.error("NI");
                        //curse(item.contents);
                    }
                    else
                    {
                        // TODO: Recognize object, too.
                        var perexElement = document.createElement("p");
                        perexElement.setAttribute("class", "blog-perex");
                        perexElement.appendChild(document.createTextNode(item["perex"]));
                        container.appendChild(perexElement);
                    }
                }
                else if (keys[0] === "p")
                {
                    if (Array.isArray(item["p"]))
                    {
                        var pElement = document.createElement("p");
                        container.appendChild(pElement);
                        curse(item["p"], pElement);
                    }
                    else
                    {
                        // TODO: Recognize object, too.
                        var pElement = document.createElement("p");
                        pElement.appendChild(document.createTextNode(item["p"]));
                        container.appendChild(pElement);
                    }
                }
                else if (keys[0] === "h2")
                {
                    if (Array.isArray(item["h2"]))
                    {
                        console.error("H2 requires non-array.");
                    }
                    else
                    {
                        // TODO: Recognize object, too.
                        var h2Element = document.createElement("h2");
                        h2Element.appendChild(document.createTextNode(item["h2"]));
                        container.appendChild(h2Element);
                    }
                }
                else if (keys[0] === "ul")
                {
                    if (Array.isArray(item["ul"]))
                    {
                        var ulElement = document.createElement("ul");
                        container.appendChild(ulElement);
                        curse(item["ul"], ulElement, "li");
                    }
                    else
                    {
                        console.error("UL requires array.");
                    }
                }
                else if (keys[0] === "i")
                {
                    if (typeof item["i"] === "string" || item["i"] instanceof String)
                    {
                        var iElement = document.createElement("i");
                        iElement.appendChild(document.createTextNode(item["i"]));
                        container.appendChild(iElement);
                    }
                    else
                    {
                        console.error("Nope: i.");
                    }
                }
                else if (keys[0] === "snippet-cs")
                {
                    if (Array.isArray(item["snippet-cs"]))
                    {
                        var preElement = document.createElement("pre");
                        container.appendChild(preElement);
                        var codeElement = document.createElement("code");
                        preElement.appendChild(codeElement);
                        codeElement.className = "cs hljs";
                        codeElement.textContent = item["snippet-cs"].join("\n");
                    }
                    else
                    {
                        console.error("SNIPPET-CS requires array.");
                    }
                }
                else
                {
                    console.error("A", keys);
                }
            }
            else if (keys.length === 2 && keys[0] === "a" && keys[1] === "url")
            {
                if (typeof item["a"] === "string" || item["a"] instanceof String)
                {
                    var aElement = document.createElement("a");
                    aElement.setAttribute("href", item["url"]);
                    aElement.appendChild(document.createTextNode(item["a"]));
                    container.appendChild(aElement);
                }
                else
                {
                    console.error("Nope: a.");
                }
            }
            else
            {
                console.error("B", keys);
            }
        }
        else
        {
            console.error(item, "is of unknown type:", typeof item);
        }
    }
}

/* Anti spam */

window.onload = function() {
    var mailUsA = document.getElementById("mailUsA");
    mailUsA.setAttribute("href", "mailto:" + mailUsA.textContent.replace(/\u200d/gi, ""));

    var callUsA = document.getElementById("callUsA");
    callUsA.setAttribute("href", "tel:" + callUsA.textContent.replace(/\u200d/gi, ""));
};

/* Smooth scroll */

var scrollTimer;

function getHandler(target)
{
    return function(event)
    {
        event.preventDefault();
        
        scrollTimer = window.clearInterval(scrollTimer) || window.setInterval(function()
        {
            var difference = target.offsetTop - document.body.scrollTop;
            if (difference == 0)
            {
                scrollTimer = window.clearInterval(scrollTimer);
            }
            else
            {
                document.body.scrollTop += [Math.floor, function () { return 0; }, Math.ceil][Math.sign(difference) + 1](difference / 10);
            }
        }, 10);
        
        return false;
    };
}

function setupScroll()
{
    var scrollToBlogHandler = getHandler(document.getElementById("blog"));
    var scrollToTopHandler = getHandler(document.getElementById("top"));

    document.getElementById("scrollToBlogA").addEventListener("click", scrollToBlogHandler);

    var elements = document.getElementsByClassName("scrollToTopA");
    for (var elementIndex = 0; elementIndex < elements.length; elementIndex++)
    {
        var element = elements[elementIndex];
        element.addEventListener("click", scrollToTopHandler);
    }

    document.body.addEventListener("scroll", function ()
    {
        // Do not hijack user's scrolling.
        scrollTimer = window.clearInterval(scrollTimer); 
    });    
}

/* Delaunay */
function drawDelaunay()
{
    var canvas = document.createElement("canvas");
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;

    var context = canvas.getContext("2d");

    var points = [];
    for (var i = 0; i < 250; i++)
    {
        points.push([ Math.random() * canvas.width * 2 - canvas.width / 2, Math.random() * canvas.height * 1.75 - canvas.height ]);
    }

    var colors = [];
    for (var i = 0; i < 50; i ++)
    {
        colors.push(230 + Math.ceil(Math.random() * 25));
    }

    var triangles = Delaunay.triangulate(points);
    for(var i = triangles.length; i; )
    {
        context.beginPath();
        context.moveTo(points[triangles[--i]][0], points[triangles[i]][1]);
        context.lineTo(points[triangles[--i]][0], points[triangles[i]][1]);
        context.lineTo(points[triangles[--i]][0], points[triangles[i]][1]);
        context.closePath();
        
        context.fillStyle = "rgb(" + colors[i % colors.length] + ", " + colors[i % colors.length] + ", " + Math.ceil(colors[i % colors.length] + Math.random() * Math.min(3, (255 - colors[i % colors.length]))) + ")";
        context.fill();
    }

    document.body.style.background = "url('" + canvas.toDataURL() + "') 50% 0 no-repeat";
}
