# [Rhaeo.net](https://Rhaeo.net/)

## Todos
- Set up and verify the redirects.
- Set up Let's Encrypt for the site.

## Redirects
- Rhaeo.cz
    - Redirects mail to Rhaeo.net
    - Redirects www to Rhaeo.net
- Rhaeo.net
    - Redirects mail to Hubelbauer.net
    - Serves www from GitLab Pages
